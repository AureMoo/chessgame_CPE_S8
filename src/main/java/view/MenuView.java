package view;


import java.util.HashMap;
import java.util.Map;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.ToggleGroup;
import view.GuiFactory;
import view.PaintStyle;
import view.command.BlackSquareCommand;
import view.command.PaintStyleCommand;
import view.command.ResetCommand;
import view.command.WhiteSquareCommand;
import view.command.undo.memento.BeforeAfter;
import view.command.undo.replay.ReplayConversation;


/**
 * @author francoise.perrin
 * Cette classe est le menu du jeu d'échec
 */
public class MenuView extends MenuBar {

	private ReplayConversation replay;
	private Map<MenuItem, PaintStyle> btnMap; 
	
	public MenuView () {
		super();

		//Le menu est le client
		
		ResetCommand commandReset = new ResetCommand(GuiFactory.paintStyle.get(), GuiFactory.whiteSquareColor.get(), GuiFactory.blackSquareColor.get());
		replay = new ReplayConversation(commandReset); //Notre invoker
		

		this.getMenus().add(newMenuStyle());
		this.getMenus().add(newMenuColor());
		this.getMenus().add(newMenuEdit());
	}

	private Menu newMenuColor () {

		Menu menu = new Menu("Couleur d'affichage");
		MenuItem color1 = new MenuItem("Couleur cases blanches");
		MenuItem color2 = new MenuItem("Couleur cases noires");

		color1.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle (ActionEvent event) {
				Dialog<Void> colorDialog = new Dialog<>();
				colorDialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);

				ColorPicker colorPicker = new ColorPicker(GuiFactory.whiteSquareColor.get());
				colorPicker.setOnAction(colorEvent -> {
										/* sans pattern Command */
										//GuiFactory.whiteSquareColor.set(colorPicker.getValue());
										replay.exec(new WhiteSquareCommand(colorPicker.getValue()));
					
					colorDialog.close();
				});

				colorDialog.getDialogPane().setContent(colorPicker);
				colorDialog.show();
			}
		});

		color2.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle (ActionEvent event) {
				Dialog<Void> colorDialog = new Dialog<>();
				colorDialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);

				ColorPicker colorPicker = new ColorPicker(GuiFactory.blackSquareColor.get());
				colorPicker.setOnAction(colorEvent -> {
										/* sans pattern Command */
										//GuiFactory.blackSquareColor.set(colorPicker.getValue());
										replay.exec(new BlackSquareCommand(colorPicker.getValue()));

					
					colorDialog.close();
				});

				colorDialog.getDialogPane().setContent(colorPicker);
				colorDialog.show();
			}
		});

		menu.getItems().addAll(color1, color2);
		return menu;
	}

	private Menu newMenuStyle () {

		Menu menu = new Menu("Style d'affichage");
		RadioMenuItem style1 = new RadioMenuItem("Dégradé");
		RadioMenuItem style2 = new RadioMenuItem("Uni");
		ToggleGroup btnGroup = new ToggleGroup();

		style1.setToggleGroup(btnGroup);
		style2.setToggleGroup(btnGroup);

		btnMap = new HashMap<>();
		btnMap.put(style1, PaintStyle.GRADIENT);
		btnMap.put(style2, PaintStyle.SOLID);

		Object style = GuiFactory.paintStyle.get();
		btnMap.forEach((menuItem, paintstyle) -> {
			if (paintstyle.equals(style)) {
				((RadioMenuItem) menuItem).setSelected(true);
			}
		});
		
		style1.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle (ActionEvent event) {
				 /* sans pattern Command */
				//GuiFactory.paintStyle.set(PaintStyle.GRADIENT);		
				replay.exec(new PaintStyleCommand(PaintStyle.GRADIENT));
			}
		});

		style2.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle (ActionEvent event) {
				 /* sans pattern Command */
				//GuiFactory.paintStyle.set(PaintStyle.SOLID);		
				replay.exec(new PaintStyleCommand(PaintStyle.SOLID));
			}
		});

		menu.getItems().addAll(style1, style2);
		return menu;
	}

	private Menu newMenuEdit () {

		Menu menu = new Menu("Gestion Commandes");
		MenuItem undo = new MenuItem("undo");
		MenuItem redo = new MenuItem("redo");

		undo.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle (ActionEvent event) {
				replay.undo();
				Object style = GuiFactory.paintStyle.get();
				btnMap.forEach((menuItem, paintstyle) -> {
					if (paintstyle.equals(style)) {
						((RadioMenuItem) menuItem).setSelected(true);
					}
				});
			}
		});

		redo.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle (ActionEvent event) {
				replay.redo();
				Object style = GuiFactory.paintStyle.get();
				btnMap.forEach((menuItem, paintstyle) -> {
					if (paintstyle.equals(style)) {
						((RadioMenuItem) menuItem).setSelected(true);
					}
				});
			}
		});

		menu.getItems().addAll(undo, redo);
		return menu;
	}


}
