package view;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import shared.GUICoord;

public class SquareGuiDecorator extends BorderPane implements ChessSquareGui {

	private SquareGui squareGui;
	
	public SquareGuiDecorator(SquareGui squareGui) {
		super();
		this.squareGui = squareGui;
		this.setCenter(this.squareGui);
		this.paint();
		
		
		GuiFactory.blackSquareColor.addListener(new ChangeListener<Color>() {
			@Override
			public void changed(ObservableValue<? extends Color> observable, Color oldValue, Color newValue) {
				paint();
			}
		});
		
		GuiFactory.whiteSquareColor.addListener(new ChangeListener<Color>() {
			@Override
			public void changed(ObservableValue<? extends Color> observable, Color oldValue, Color newValue) {
				paint();
			}
		});
		
		GuiFactory.paintStyle.addListener(new ChangeListener<PaintStyle>() {
			@Override
			public void changed(ObservableValue<? extends PaintStyle> observable, PaintStyle oldValue, PaintStyle newValue) {
				paint();
			}
		});
	}
	
	@Override
	public GUICoord getCoord() {
		return this.squareGui.getCoord();
	}

	@Override
	public void resetColor(boolean isLight) {
		this.squareGui.resetColor(isLight);
		paint();
	}

	@Override
	public void paint() {
		this.squareGui.paint();
	}


}
