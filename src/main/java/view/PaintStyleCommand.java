package view;

import view.command.Command;

public class PaintStyleCommand implements Command {
	
	private PaintStyle paintStyle;
	
	public PaintStyleCommand(PaintStyle paintStyle ) {
		this.paintStyle = paintStyle;
	}

	@Override
	public void execute() {
		GuiFactory.paintStyle.set(this.paintStyle);
	}
}