package view.command;

import javafx.scene.paint.Color;
import view.GuiFactory;

public class WhiteSquareCommand implements Command {
	
	private Color colorWhitePiece;
	
	public WhiteSquareCommand(Color colorWhitePiece) {
		this.colorWhitePiece = colorWhitePiece;
	}

	@Override
	public void execute() {
		GuiFactory.whiteSquareColor.set(colorWhitePiece);
	}

}
