package view.command;

@FunctionalInterface
public interface Command {
	void execute();
}
