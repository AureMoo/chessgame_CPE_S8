package view.command.undo.compensation;

import view.command.Command;

public interface CompensableCommand extends Command {
	void compensate();
}
