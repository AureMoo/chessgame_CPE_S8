package view.command;

import javafx.scene.paint.Color;
import view.GuiFactory;
import view.PaintStyle;

public class ResetCommand implements Command {
	
	private PaintStyle paintStyle;
	private Color colorWhitePiece;
	private Color colorBlackPiece;
	
	
	public ResetCommand(PaintStyle paintStyle, Color colorWhitePiece, Color colorBlackPiece ) {
		this.paintStyle = paintStyle;
		this.colorWhitePiece = colorWhitePiece;
		this.colorBlackPiece = colorBlackPiece;
	}

	@Override
	public void execute() {
		GuiFactory.paintStyle.set(paintStyle);
		GuiFactory.blackSquareColor.set(colorBlackPiece);
		GuiFactory.whiteSquareColor.set(colorWhitePiece);
	}

}
