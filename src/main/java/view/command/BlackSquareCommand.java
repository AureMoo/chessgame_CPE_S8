package view.command;

import javafx.scene.paint.Color;
import view.GuiFactory;

public class BlackSquareCommand implements Command {

	private Color colorBlackPiece;
	
	public BlackSquareCommand(Color colorBlackPiece) {
		this.colorBlackPiece = colorBlackPiece;
	}
	
	@Override
	public void execute() {
		GuiFactory.blackSquareColor.set(colorBlackPiece);
	}

}
